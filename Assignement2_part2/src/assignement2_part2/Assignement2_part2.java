/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;



/**
 *
 * @author graham
 */
public class Assignement2_part2 extends Application{
     
//    public static ArrayList<ClusterDataEntry> clusterList = new ArrayList<>();
    
 
    public static void main(String[] args) {
        launch(args);
        
    }
    
    

 
    @Override 
    public void start(Stage clusterGraphStage) {
         
        
        MyClusterLibrary objMyClstLib = new MyClusterLibrary();
        Scanner inputCluster = objMyClstLib.OpenFile("Cluster.txt");
        ArrayList<ClusterDataEntry> clusterList = new ArrayList<>();
        
        try{
            inputCluster.nextLine();    //removal of headding line.
            clusterList = MyClusterLibrary.ReadToArray(inputCluster);
            
        }catch(NoSuchElementException e){
            System.err.println("The cluster.txt file is empty please check the file before starting again...");
        }
 
        clusterGraphStage.setTitle("Assignement2_part2 Cluster Scatterplot");
        final NumberAxis xAxis = new NumberAxis(0, 10, 0.5);
        final NumberAxis yAxis = new NumberAxis(0, 10, 0.5);        
        final ScatterChart<Number,Number> scatterGraph = new ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("X axis");
        yAxis.setLabel("Y axis");
        scatterGraph.setTitle("Cluster Records scatterplot");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Cluster1");
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Cluster2");
        XYChart.Series series3 = new XYChart.Series<>();
        series3.setName("Cluster3");
        XYChart.Series series4 = new XYChart.Series();
        series4.setName("Cluster4");
        
        double xVal;
        double yVal;
        String clstrNum;
        Object xyChartDataVal;
        
        for (ClusterDataEntry clusterVal : clusterList){
            xVal = clusterVal.X;
            yVal = clusterVal.Y;
            clstrNum = clusterVal.Clstr;
            xyChartDataVal = new XYChart.Data(xVal, yVal);
            
            if(null != clstrNum)switch (clstrNum) {
                case "Cluster1":
                    series1.getData().add(xyChartDataVal);
                    break;
                case "Cluster2":
                    series2.getData().add(xyChartDataVal);
                    break;
                case "Cluster3":
                    series3.getData().add(xyChartDataVal);
                    break;
                case "Cluster4":
                    series4.getData().add(xyChartDataVal);
                    break;
                default:
                    break;
            }
        }
        
        scatterGraph.getData().addAll(series1, series2, series3, series4);
        Scene scene  = new Scene(scatterGraph, 800, 600);
        clusterGraphStage.setScene(scene);
        clusterGraphStage.show();
    }
}
