/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author graham
 */
public class MyClusterLibrary {
    
    
    
    public Scanner input;
        
    public Scanner OpenFile(String txtFile)
    {
        // attempt to open/read file and catch errors.
        try 
        {
            input = new Scanner(new File(txtFile));
            
            System.out.println("The file :" + txtFile + " has been read successfully.");
            System.out.println();
        } 
        catch (FileNotFoundException e) 
        {
            System.err.println("\t\tError - File: " + txtFile + " - dos'nt exist or not in directory \n" 
                    + "\t\tPlease check the filename and path before trying again.");
            System.out.println();
            System.out.println("Error is - " + e);
            System.err.flush();
                    // note not using the "e" to stop program stopping.
            System.out.println();
        }
        return input;
    }
    
    
     public static ArrayList ReadToArray(Scanner scannerInput){
        
        ArrayList<ClusterDataEntry> arrayReturn = new ArrayList<>();
        
        double xVal;
        double yVal;
        String clusterNum;
        ClusterDataEntry arrayEntry;
        
        
        while(scannerInput.hasNextLine()){
            xVal = scannerInput.nextDouble();
            yVal = scannerInput.nextDouble();
            clusterNum = scannerInput.next();
            arrayEntry = new ClusterDataEntry(xVal, yVal, clusterNum);
            arrayReturn.add(arrayEntry);
        }
        
//        }
        return arrayReturn;
    }
    
}
