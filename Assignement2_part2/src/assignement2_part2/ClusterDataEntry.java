/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part2;

import java.text.DecimalFormat;


/**
 *
 * @author graham
 */
public class ClusterDataEntry {
    
    DecimalFormat df = new DecimalFormat("##0.000");
    
    double X;
    double Y;
    String Clstr;
    
    
    public ClusterDataEntry(double X, double Y, String Clstr){
        this.X = Double.valueOf(df.format(X));
        this.Y = Double.valueOf(df.format(Y));
        this.Clstr = Clstr;
    }
}
