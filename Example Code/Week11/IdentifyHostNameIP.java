/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week11;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author MdAnisur
 */
public class IdentifyHostNameIP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        for(int i=0;i<args.length;i++){
            try{
                InetAddress address= InetAddress.getByName(args[i]);
                System.out.printf("Host Name:"+address.getHostName());
                System.out.printf("IP Address:"+address.getHostAddress());
                
            }
            catch(UnknownHostException ex){
                System.out.println("Unknown Host:"+args[i]);
                
            }
            
        }
    }
    
}
