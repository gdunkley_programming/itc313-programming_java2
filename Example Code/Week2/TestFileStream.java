package Week2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 60065038
 */
import java.io.*;

public class TestFileStream {
  public static void main(String[] args) throws IOException {
    // Create an output stream to the file
    FileOutputStream output = new FileOutputStream("javed.dat", true);

//    // Output values to the file
    for (int i = 1; i <= 10; i++)
      output.write(i);

    // Close the output stream
    output.close();

    // Create an input stream for the file
    FileInputStream input = new FileInputStream("javed.dat");

    // Read values from the file
    int value;
    //System.out.println("has data ?"+input.read());
    while ((value = input.read()) != -1)
      System.out.print(value + " ");

    // Close the output stream
    input.close();
  }
}
