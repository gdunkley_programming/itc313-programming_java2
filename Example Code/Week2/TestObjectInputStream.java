/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week2;

/**
 *
 * @author 60065038
 */
import java.io.*;

public class TestObjectInputStream {
  public static void main(String[] args)
    throws ClassNotFoundException, IOException {
    // Create an input stream for file object.dat
    ObjectInputStream input =new ObjectInputStream(new FileInputStream("javed.dat"));

    // Write a string, double value, and object to the file
    String name = input.readUTF();
    double score = input.readDouble();
    java.util.Date date = (java.util.Date)(input.readObject());
    System.out.println(name + " " + score + " " + date);

    // Close input stream
    input.close();
  }
}