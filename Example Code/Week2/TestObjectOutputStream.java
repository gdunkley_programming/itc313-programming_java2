/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week2;

import java.io.*;

public class TestObjectOutputStream {
  public static void main(String[] args) throws IOException {
    // Create an output stream for file object.dat
    ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("javed.dat"));

    // Write a string, double value, and object to the file
    output.writeUTF("John");
    output.writeDouble(85.5);
    output.writeObject(new java.util.Date());

    // Close output stream
    output.close();
  }
}