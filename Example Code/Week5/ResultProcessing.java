/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week5;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author 60065038
 */
public class ResultProcessing extends Application {
    
    @Override
    public void start(Stage primaryStage) {   
        
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        
        Scene scene = new Scene(pane, 250, 250);
        
        
        Label name = new Label("Name");
        pane.add(name, 0, 1);
        TextField nameField = new TextField();
        nameField.setPromptText("Enter Name");
        pane.add(nameField, 1, 1);
        
        
        Label marks = new Label("Raw Marks");
        pane.add(marks, 0, 2);        
        TextField marksField = new TextField();
        marksField.setPromptText("Marks should be in between 0 and 100");
        pane.add(marksField, 1, 2);
        
        Label weight = new Label("Weight in %100");
        pane.add(weight,0,3);
        TextField weightField = new TextField();
        pane.add(weightField, 1, 3);

        Button calculate = new Button("Calculate");        
        HBox hbox = new HBox(10);
        hbox.setAlignment(Pos.BOTTOM_RIGHT);
        hbox.getChildren().add(calculate);
        pane.add(hbox, 1, 4);

        Text taxMessage = new Text();
        pane.add(taxMessage, 1, 7);
        
        //.................................................
        
        Label WMarks = new Label("Weighted Marks");
        pane.add(WMarks, 0, 5);        
        TextField WMarksField = new TextField();
        pane.add(WMarksField, 1, 5);
        
        
        //...............................................

        calculate.setOnAction(new EventHandler<ActionEvent>() {
            
            public void handle(ActionEvent event) {
                Double RawMarks = Double.parseDouble(marksField.getText());
                Double Weight = Double.parseDouble(weightField.getText())/100;
                Double results=RawMarks*Weight;
                taxMessage.setText(nameField.getText()+" obtained: "+results);
               
                
                WMarksField.setText(String.valueOf(results));
            }
        });
        
      
       
       
        
        primaryStage.setTitle("Result Processing");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
