package Week6;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GradeProcessing extends Application {
  private TextField ID = new TextField();
  private TextField Name = new TextField();
  private TextField Marks = new TextField();
  private TextField Weight = new TextField();
  private TextField Grade = new TextField();
  private TextField WeightedMarks = new TextField();
  private Button btCalculate = new Button("Calculate Grades");
  
  @Override // Override the start method in the Application class
  public void start(Stage primaryStage) {
    // Create UI
    GridPane gridPane = new GridPane();
    gridPane.setHgap(5);
    gridPane.setVgap(5);
    gridPane.add(new Label("Student ID:"), 0, 0);
    gridPane.add(ID, 1, 0);
    gridPane.add(new Label("Student Name :"), 0, 1);
    gridPane.add(Name, 1, 1);
    gridPane.add(new Label("Marks :"), 0, 2);
    gridPane.add(Marks, 1, 2);
    
    gridPane.add(new Label("Weight of the Assessment :"), 0, 3);
    gridPane.add(Weight, 1, 3);
    
    gridPane.add(new Label("Grade :"), 0, 4);
    gridPane.add(Grade, 1, 4);
    
    gridPane.add(new Label("Weighted Marks:"), 0, 5);
    gridPane.add(WeightedMarks, 1, 5);

    gridPane.add(btCalculate, 1, 7);

    //Set properties for UI
    gridPane.setAlignment(Pos.CENTER);
    ID.setAlignment(Pos.BOTTOM_RIGHT);
    Name.setAlignment(Pos.BOTTOM_RIGHT);
    Marks.setAlignment(Pos.BOTTOM_RIGHT);
    Weight.setAlignment(Pos.BOTTOM_RIGHT);
    Grade.setAlignment(Pos.BOTTOM_RIGHT);
    
    WeightedMarks.setAlignment(Pos.BOTTOM_RIGHT);
    WeightedMarks.setAlignment(Pos.BOTTOM_RIGHT);
    WeightedMarks.setEditable(false);
    Grade.setEditable(false);
    GridPane.setHalignment(btCalculate, HPos.RIGHT);

    // Process events
    btCalculate.setOnAction(e -> CalculateGrade());

    // Create a scene and place it in the stage
    Scene scene = new Scene(gridPane, 400, 250);
    primaryStage.setTitle("Grade Processing"); // Set title
    primaryStage.setScene(scene); // Place the scene in the stage
    primaryStage.show(); // Display the stage
  }
  
  private void CalculateGrade() {
    // Get values from text fields
    //String SID =ID.getText();
    double weight=Double.parseDouble(Weight.getText());
    double SMark =Double.parseDouble(Marks.getText());
    double WMark=SMark*weight;
    String garde="";
   
    if(WMark<50){
       garde="Fail"; 
    }
    else if(WMark>=50 && WMark<65){
         garde="Pass"; 
    }
    
    else if(WMark>=65 && WMark<75){
         garde="CR"; 
    }
    else if(WMark>=75 && WMark<85){
         garde="DI"; 
    }
    else
         garde="HD"; 
    
    WeightedMarks.setText(String.valueOf(WMark));
    Grade.setText(garde);
  
  }
  
  /**
   * The main method is only needed for the IDE with limited
   * JavaFX support. Not needed for running from the command line.
   */
  public static void main(String[] args) {
    launch(args);
  }
}