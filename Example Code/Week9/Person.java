package Week9;

import Assignment1.*;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

// Used to create a person object for processing student grades
public class Person {
		 
		private final SimpleStringProperty name;
		private final SimpleStringProperty ID;
		private final SimpleIntegerProperty quizMark;
		private final SimpleIntegerProperty a1;
		private final SimpleIntegerProperty a2;
		private final SimpleIntegerProperty exam;
		private final SimpleDoubleProperty results;
		private final SimpleStringProperty grade;
 

		
        Person(String name,
        		String ID,
        		int quizMark,
        		int a1,
        		int a2,
        		int exam,
        		double results,
        		String grade) {
        
        	this.name = new SimpleStringProperty(name);
        	this.ID = new SimpleStringProperty(ID);
        	this.quizMark = new SimpleIntegerProperty(quizMark);
        	this.a1 = new SimpleIntegerProperty(a1);
        	this.a2 = new SimpleIntegerProperty(a2);
        	this.exam = new SimpleIntegerProperty(exam);
        	this.results = new SimpleDoubleProperty(results);
        	this.grade = new SimpleStringProperty(grade);        	
        }



		public String getName() {
			return name.getValue();
		}

		public String getID() {
			return ID.getValue();
		}

		public int getQuizMark() {
			return quizMark.getValue().intValue();
		}

		public int getA1() {
			return a1.getValue().intValue();
		}

		public int getA2() {
			return a2.getValue().intValue();
		}

		public int getExam() {
			return exam.getValue().intValue();
		}
		
		
		

		public double getResults() {
			return results.getValue().doubleValue();
		}
		
		public void setResults(double value) {
			results.set(value);
		}
		
		public DoubleProperty resultsProperty() {
			return results;
		}



		public String getGrade() {
			return grade.get();
		}
		
		public void setGrade(String value) {
			grade.set(value);
		}
		
		public StringProperty gradeProperty() {
			return grade;
		}   
    }