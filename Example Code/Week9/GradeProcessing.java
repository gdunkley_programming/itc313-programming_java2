package Week9;

import Assignment1.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.geometry.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class GradeProcessing extends Application {
	
//	 private TableView<Person> table = new TableView<Person>();
	    private final ObservableList<Person> data =
	        FXCollections.observableArrayList(
	        		// sample data from design spec for testing purposes
	        		new Person("TEST", "12345678",50,50,50,50,0.0,""),
	        		new Person("XYZ", "11516158",90,56,67,88,0.0,"")
	        		);
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void start (Stage primaryStage) {
	
		// set up pane
		GridPane entryPane = new GridPane();
		// set up pane alignment
		entryPane.setAlignment(Pos.CENTER);
		
		//set up borders on pane object
		entryPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5 ));
		entryPane.setHgap(5.5);
		entryPane.setVgap(5.5);
		
		// set up TextField objects
		TextField txtStudentID = new TextField();
		TextField txtStudentName = new TextField();
		TextField txtQuizMarks = new TextField();
		TextField txtAssOne = new TextField();
		TextField txtAssTwo = new TextField();
		TextField txtExam = new TextField();
		TextField txtAverageMark = new TextField();
		txtAverageMark.setEditable(false);
		
		// check to make sure student ID is a valid
		txtStudentID.textProperty().addListener((observable, oldValue, newValue) -> {
		    try {
		    	if (newValue.equals("")) {
		    		return;
		    	}
		    	
		    	// make sure it is made up of integer values even though it a String field
				Integer.parseInt(newValue);
				
				// make sure it is 8 digits long
				if (newValue.length() > 8) {
					throw new NumberFormatException();
				}
				
		    } catch (NumberFormatException e) {
		    	txtStudentID.setText(oldValue);
		    }
		});
		
		
		//this.setUpTextFieldAsInteger(txtStudentID);
		this.setUpTextFieldAsInteger(txtQuizMarks);
		this.setUpTextFieldAsInteger(txtAssOne);
		this.setUpTextFieldAsInteger(txtAssTwo);
		this.setUpTextFieldAsInteger(txtExam);
		
		// place nodes in the pane
		entryPane.add(new Label("Student ID (must be 8 digits)"), 0, 0);
		entryPane.add(txtStudentID, 1, 0);
		entryPane.add(new Label("Student Name"), 0, 1);
		entryPane.add(txtStudentName, 1, 1);
		entryPane.add(new Label("Quiz Marks"), 0, 2);
		entryPane.add(txtQuizMarks, 1, 2);
		entryPane.add(new Label("Assignment 1 Marks (enter 0-100)"), 0, 3);
		entryPane.add(txtAssOne, 1, 3);
		entryPane.add(new Label("Assignment 2 Marks (enter 0-100)"), 0, 4);
		entryPane.add(txtAssTwo, 1, 4);
		entryPane.add(new Label("Exam Marks (enter 0-100)"), 0, 5);
		entryPane.add(txtExam, 1, 5);
		
		
		// Create a table of Person objects
		TableView<Person> table = new TableView<Person>();
		
		// set up columns
		table.getColumns().addAll(
				makeStringColumn("Name", "name"),
				makeStringColumn("ID", "ID"),
				makeIntColumn("Quiz Mark", "quizMark"),
				makeIntColumn("A1", "a1"),
				makeIntColumn("A2", "a2"),
				makeIntColumn("Exam", "exam"),
				makeDoubleColumn("Results", "results"),
				makeStringColumn("Grade", "grade")
				);
		
		// needed to prevent extra columns added in with unused header space
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		table.setItems(data);
		
		// set up pane
		GridPane buttonPane = new GridPane();
		
		//set up pane alignment
		buttonPane.setAlignment(Pos.CENTER);
		buttonPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5 ));
		buttonPane.setHgap(5.5);
		buttonPane.setVgap(5.5);
		
		// create buttons
		Button averageMarksButton = new Button("Average Marks");
		Button addStudentMarksButton = new Button("Student Marks");
		Button addNewStudentButton = new Button("Add New Student");
		
		// set up event fp average marks button
		averageMarksButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        
		    	if (data.size() == 0) {
		    		return;
		    	}
		    	
		    	// calculate average
		    	double average = 0.0;
		    	for (Person p : data) {
		    		average += p.getResults();
		    	}
		    	
		    	average = average / data.size();
		    	
		    	// round average to 2 decimal places
		    	DecimalFormat df = new DecimalFormat("#.##");
		    	df.setRoundingMode(RoundingMode.HALF_UP);
		    	String formattedAverage = df.format(average);
		    	
		    	txtAverageMark.setText(formattedAverage);
		    	
		    }   
		});
		
		// set up events for the student marks button
		addStudentMarksButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        
		    	for (Person p : data) {
		    		
		    		// calculate overall results
		    		double results = (p.getQuizMark() * 0.05) + (p.getA1() *0.2) + (p.getA2() * 0.25) + (p.getExam() * 0.5);
			    	String grade = calculateGradeFromMark(results);		    	
		    		
		    		p.setResults(results);
		    		p.setGrade(grade);
		    	}
		    }   
		});
		
		// set up events for add new student button
		addNewStudentButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        
		    	try {
		    	String name = txtStudentName.getText();
		    	String ID = txtStudentID.getText();
		    	
		    	if (ID.length() < 8){
		    		throw new NumberFormatException();
		    		
		    	}
		    	
		    	int quizMark = Integer.parseInt(txtQuizMarks.getText()); 
		    	int assOne = Integer.parseInt(txtAssOne.getText());
		    	int assTwo = Integer.parseInt(txtAssTwo.getText());
		    	int exam = Integer.parseInt(txtExam.getText());
		    	
		    	Person p = new Person(name, ID, quizMark, assOne, assTwo, exam, 0.0, null);
		        data.add(p);
		        
		        // clear TextFields after student has been added to the list
		        txtStudentID.clear();
				txtStudentName.clear();
				txtQuizMarks.clear();
				txtAssOne.clear();
				txtAssTwo.clear();
				txtExam.clear();
				txtAverageMark.clear();
		        
		    	} catch (NumberFormatException ex){
		    		
		    		// display error message  when student ID is not valid
		    		Alert alert = new Alert(Alert.AlertType.ERROR);
		    		alert.setTitle("Data Entry Error");
		    		alert.setHeaderText("Invalid Value Entered");
		    		alert.setContentText("Student ID must be numeric and 8 digits long");
		    		alert.showAndWait();
		    	}
		    }   
		});
		
		

		// place nodes in the pane
		buttonPane.add(new Label("Average Marks"), 0, 0);
		buttonPane.add(txtAverageMark, 1, 0);
		buttonPane.add(averageMarksButton, 2, 0);
		buttonPane.add(addNewStudentButton , 3, 0);
		buttonPane.add(addStudentMarksButton, 4, 0);
		
		GridPane containerPane = new GridPane();
		containerPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5 ));
		containerPane.setHgap(5.5);
		containerPane.setVgap(5.5);
	
		containerPane.setAlignment(Pos.CENTER);
		containerPane.add(entryPane, 0, 0);
		containerPane.add(table, 0, 1);
		containerPane.add(buttonPane, 0, 2);
		
		// Set up scene/window
		Scene scene = new Scene(containerPane);
		
		primaryStage.setTitle("Grade Processing-Programming in Java 2"); // title bar text
		primaryStage.setScene(scene);
		primaryStage.show();
		

	}

	// method to calculate the grades from the overall results
	private String calculateGradeFromMark(double mark) {
		
		String grade;
		
		if(mark >= 85){
			grade = "HD";
		} else if (mark >= 75){
			grade = "DI";
		} else if (mark >= 65){
			grade = "CR";
		} else if (mark >= 50){
			grade = "PS";
		} else {
			grade = "FL";
		}
		
		return grade;
	}

	// method to make table columns with String data type
	private TableColumn<Person, String> makeStringColumn(String header, String fieldName) {
		
		TableColumn<Person, String> column = new TableColumn<Person, String>(header);
		column.setCellValueFactory(
				new PropertyValueFactory<Person, String>(fieldName)
				);
		
		return column;	
		
	}
	
	// method to make table columns with integer data type	
	private TableColumn<Person, Integer> makeIntColumn(String header, String fieldName) {
		TableColumn<Person, Integer> column = new TableColumn<Person, Integer>(header);
		column.setCellValueFactory(
				new PropertyValueFactory<Person, Integer>(fieldName)
				);
		
		return column;	
	}
	
	// method to make table columns with double data type
	private TableColumn<Person, Double> makeDoubleColumn(String header, String fieldName) {
		
		TableColumn<Person, Double> column = new TableColumn<Person, Double>(header);
		column.setCellValueFactory(
				new PropertyValueFactory<Person, Double>(fieldName)
				);
		
		return column;	
		
	}
	
	// Method to convert TextFields from String to Integer
	private void setUpTextFieldAsInteger (TextField textField ) {
		
		textField.textProperty().addListener((observable, oldValue, newValue) -> {
		    try {
		    	if (newValue.equals("")) {
		    		return;
		    	}

				int grade = Integer.parseInt(newValue);
				
				// force values to be between 0 and 100
				if (grade < 0 || grade > 100) {
					throw new NumberFormatException();
				}
				
		    } catch (NumberFormatException e) {
		    	// if value not equal to an int then revert to old value or null
		    	textField.setText(oldValue);
		    }
		});
		
		
	}

	
	// Main method to run application in IDE
	public static void main(String[] args) {
		Application.launch(args);
	}
	
}
