package Week9;

import java.sql.*;

public class SimpleJdbc {
  public static void main(String[] args)
      throws SQLException, ClassNotFoundException {
    // Load the JDBC driver
    Class.forName("com.mysql.jdbc.Driver");
    System.out.println("Loaded the MySQL JDBC Driver");

    // Connect to a database
    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/gradeprocessing" , "root", "admin");
    System.out.println("Successfully connected to Database");

    // Create a statement
    Statement statement = connection.createStatement();
    
//    String sql = "INSERT INTO Student " + "VALUES (5, 'Michael', 'ITC521', 80)";
//    statement.execute(sql);
//    sql ="INSERT INTO Student " + "VALUES (4, 'John', 'ITC313', 90)";
//    statement.execute(sql);
    
    
//    String sql = "INSERT INTO ITC521 " + "VALUES (3, 'Dipu', 100, 100, 100, 100, 100, 100, 'HD')";
//    statement.execute(sql);
//    sql ="INSERT INTO ITC521 " + "VALUES (4, 'John', 100, 100, 100, 100, 100, 100, 'HD')";
//    statement.execute(sql);

    // Execute a statement
    ResultSet resultSet = statement.executeQuery ("select * from ITC521");
      //("select firstName, mi, lastName from st11538531 where lastName "
      //  + " = 'Smith'");
            
    //System.out.println("ID"+ "\t" + "Name" + "\t" + "Subject" +"\t" + "Marks");
    System.out.println("ID"+ "\t" + "Name" + "\t" + "Quiz" +"\t" + "Assignment1"+"\t" + "Assignment2" +"\t" +"Assignment3"+"\t" + "Exam" +"\t"+"Result"+"\t" + "Grade");

    String result="";
    ResultSetMetaData rsmd;
    int columnsNumber=0;
    // Iterate through the result and print the student names
    while(resultSet.next()){
        rsmd = resultSet.getMetaData();
        columnsNumber = rsmd.getColumnCount();
        for(int i=1;i<=columnsNumber;i++){            
           result+=resultSet.getString(i) + "\t";      
        }
        System.out.println(result);
        result="";
      
    }
    // Close the connection
    connection.close();
  }
}
