/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week1;

/**
 *
 * @author 60065038
 */
public class Overriding {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        AA a= new AA();
        a.p(10);
        a.p(10.0);
    }
    
}

class BB{
    public void p(double i){
        System.out.println(i*2);

    }

}

class AA extends BB{
    public void p(double i){
        System.out.println(i);

    }

}
