package Week1;

public class WriteData {
  public static void main(String[] args) throws Exception {
    java.io.File file = new java.io.File("test.txt");
    if (file.exists()) {
      System.out.println("File already exists");
      System.exit(0);
    }

    // Create a file
    java.io.PrintWriter output = new java.io.PrintWriter(file);

    // Write formatted output to the file
    output.print("Ned M M ");
    output.println(95);
    output.print("Stewart K AAAAA ");
    output.println(85);

    // Close the file
    output.close();
  }
}
