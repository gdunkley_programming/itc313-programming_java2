/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Week1;

/**
 *
 * @author 60065038
 */
public class Overloading {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        AAA a= new AAA();
        a.p(10);
        a.p(10.0);
    }
    
}

class BBB{
    public void p(double i){
        System.out.println(i*2);

    }

}

class AAA extends BBB{
    public void p(int i){
        System.out.println(i);

    }

}
