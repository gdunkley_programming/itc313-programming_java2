package Week4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class ShowCircle extends Application {
  @Override // Override the start method in the Application class
  public void start(Stage primaryStage) {
    // Create a circle and set its properties
    
    Pane pane = new Pane();
    
    for(int i=1;i<=10;i++){
        Circle circle = new Circle();
//        circle.setCenterX(20);
//        circle.setCenterY(20);
       circle.setCenterX(20+i*5);
        circle.setCenterY(20+i*10);
        circle.setRadius(5);
        circle.setStroke(Color.RED);
        circle.setFill(Color.BLUE);

        // Create a pane to hold the circle 

        pane.getChildren().add(circle);
    }
    // Create a scene and place it in the stage
    Scene scene = new Scene(pane, 500, 400);
    primaryStage.setTitle("Show Circle"); // Set the stage title
    primaryStage.setScene(scene); // Place the scene in the stage
    primaryStage.show(); // Display the stage
  }
  
  /**
   * The main method is only needed for the IDE with limited
   * JavaFX support. Not needed for running from the command line.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
