
package assignement2_part3;


import javafx.geometry.Insets;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import javafx.stage.*;







public class Assignement2_part3 extends Application {
    
    //Declaring String Array for font type and size
    private String[] fontTypes = {"Times New Roman", "Cambria", "Constantia",
        "Georgia", "Palatino", "Arial", "Calibri", "Consolas", "Corbel", "Lucida Sans"};
    private String[] fontSize = {"8", "12", "16", "24", "30", "36", "42", "48", "56"};
//    private double[] fontSize = {8, 12, 16, 24, 30, 36, 42, 48, 56};
    
    private ComboBox cboFont = new ComboBox();
    private ComboBox cboFontSize = new ComboBox();
    private Label label = new Label("Programming is fun");
    private CheckBox chkBoxBold = new CheckBox(" Bold   ");
    private CheckBox chkBoxItalic = new CheckBox(" Italic ");
    private FontWeight bold;
    private FontPosture italic;
        
    protected BorderPane getBorderPane() {
        
        BorderPane bPane = new BorderPane();
        
        Font fontNormal = Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 36);
        label.setFont(fontNormal);
        
        HBox topFontBox = new HBox();
        Label fontTypeLabel = new Label("Font Name ");
        Label fontSizeLabel = new Label("   Size ");
        topFontBox.setPadding(new Insets(5, 5, 5, 5));
        topFontBox.getChildren().addAll(fontTypeLabel, cboFont, fontSizeLabel, cboFontSize);
        topFontBox.setAlignment(Pos.CENTER);
        cboFont.setPrefWidth(200);
        cboFont.setValue(fontTypes[0]);
        cboFontSize.setValue(fontSize[5]);
        
        ObservableList<String> obserListFont = FXCollections.observableArrayList(fontTypes);        
        cboFont.getItems().addAll(obserListFont);
        ObservableList<String> obserListSize = FXCollections.observableArrayList(fontSize);
        cboFontSize.getItems().addAll(obserListSize);
        bPane.setTop(topFontBox);
        
        
        
        HBox bottomCheckBoxPane = new HBox();
        bottomCheckBoxPane.setPadding(new Insets(5, 5, 5, 5));

        bottomCheckBoxPane.getChildren().addAll(chkBoxBold, chkBoxItalic);
        bottomCheckBoxPane.setAlignment(Pos.CENTER);
        bPane.setBottom(bottomCheckBoxPane);

//        label.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 56));
        
//        label.setFont(Font.font("Palatino"));
        
        
        // Display the selected font and size
        cboFont.setOnAction(e -> {
            setLabelFont(bold, italic);
        });
        
        cboFontSize.setOnAction(e -> {
            setLabelFont(bold, italic);
        });
        
        chkBoxBold.setOnAction(e -> {
            if (chkBoxBold.isSelected()){
                bold = FontWeight.BOLD;
//                System.out.println("chkBoxBold Ticked");
            } else {
                bold = null;
            }            
            setLabelFont(bold, italic);
        });
        
        chkBoxItalic.setOnAction(e -> {
            if (chkBoxItalic.isSelected()){
                italic = FontPosture.ITALIC;
//                System.out.println("chkBoxItalic Ticked");
            } else {
                italic = null;
            }
            setLabelFont(bold, italic);
        });
        
        bPane.setCenter(label);
        return bPane;
    }
    
    public void setLabelFont(FontWeight boldVal, FontPosture italicVal){
        String strFont = cboFont.getValue().toString();
        double dblSize = Double.parseDouble(cboFontSize.getValue().toString());
        
//        System.out.println(strFont + " - setLabelFont");
//        System.out.println(dblSize + " - setLabelFont");
//        label.setFont(Font.font(strFont, dblSize));
        label.setFont(Font.font(strFont, bold, italic, dblSize));
        
    }
    
    
    
    @Override
    public void start(Stage primaryStage)  {
        Scene scene = new Scene(getBorderPane(), 560, 150);
        primaryStage.setTitle("Assignement2 part3");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
    public static void main (String[] args){
        launch(args);
    }
}