/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author graham
 */
public class Tuna implements Runnable {
    String name;
    int time;
    Random r = new Random();
    
    public Tuna(String x) {
        name = x;
        time = r.nextInt(999);
    }
    
    public void run(){
        try{
            //code that runs when you start the thread
            System.out.printf("%s us sleeping for %d\n", name, time);
            Thread.sleep(time);
            System.out.printf("%s is done\n", name);
            
        } catch (Exception e) {
            
        }
    }
    
}
