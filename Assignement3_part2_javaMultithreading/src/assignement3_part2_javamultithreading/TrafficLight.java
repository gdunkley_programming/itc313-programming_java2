package assignement3_part2_javamultithreading;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author graham
 */
public class TrafficLight {
    
    Circle circle1 = new Circle();
    Circle circle2 = new Circle();
    Circle circle3 = new Circle();
    
    public Pane TrafficLight() {
        
        Pane pane = new Pane();
        Rectangle rec = new Rectangle(5, 15, 375, 156);
        rec.setArcWidth(100);
        rec.setArcHeight(100);
        pane.getChildren().add(rec);
        
        circle1.setStroke(Color.WHITE);
        circle2.setStroke(Color.WHITE);
        circle3.setStroke(Color.WHITE);
        circle1.setFill(Color.GREY);
        circle2.setFill(Color.GREY);
        circle3.setFill(Color.GREY);
        
        circle1.setCenterX(90);
        circle1.setCenterY(93);
        circle1.setRadius(45);
        
        circle2.setCenterX(195);
        circle2.setCenterY(93);
        circle2.setRadius(45);
        
        circle3.setCenterX(300);
        circle3.setCenterY(93);
        circle3.setRadius(45);
        
        pane.getChildren().add(circle1);
        pane.getChildren().add(circle2);
        pane.getChildren().add(circle3);
        
        return pane;
    }
    
    public void setColor(String color) {
        if ("red".equals(color)) {
            circle1.setFill(Color.GREY);
            circle2.setFill(Color.GREY);
            circle3.setFill(Color.RED);
        }
        if ("yellow".equals(color)) {
            circle1.setFill(Color.GREY);
            circle2.setFill(Color.YELLOW);
            circle3.setFill(Color.GREY);
        }
        if ("green".equals(color)) {
            circle1.setFill(Color.GREEN);
            circle2.setFill(Color.GREY);
            circle3.setFill(Color.GREY);
        }
    }
    
    
}
