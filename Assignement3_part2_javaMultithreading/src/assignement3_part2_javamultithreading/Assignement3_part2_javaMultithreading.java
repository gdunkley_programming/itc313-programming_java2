package assignement3_part2_javamultithreading;

import javafx.application.Application;
import javafx.stage.*;


public class Assignement3_part2_javaMultithreading extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        
        NewStage stage1 = new NewStage("Stage1");
        NewStage stage2 = new NewStage("Stage2");
        NewStage stage3 = new NewStage("Stage3");
//
        stage1.setGreen();
        stage2.setYellow();
        stage3.setRed();
        
    }
}