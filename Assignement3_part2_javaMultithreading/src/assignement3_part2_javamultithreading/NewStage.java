
package assignement3_part2_javamultithreading;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;



/**
 *
 * @author graham
 */
public class NewStage {
    
    TrafficLight traffLight = new TrafficLight();
    TextArea msgTxtField = new TextArea();   
    TextField txtgreen = new TextField();
    TextField txtyellow = new TextField();
    TextField txtred = new TextField();
    int rVal, yVal, gVal;
    boolean state1, state2;
    
    private Thread thread = null;
    private RunThreads runnable = null;
      
    public NewStage(String name){
        Stage subStage = new Stage();        
        Scene scene = new Scene(getBorderPane(), 500, 400);
        subStage.setTitle(name);
        subStage.setScene(scene);
        subStage.show();
        
    }
    
    
    protected BorderPane getBorderPane() {
        // set panes
        BorderPane bpane = new BorderPane();
        GridPane lightPane = new GridPane();
        GridPane buttonPane = new GridPane();
        
        // configure pane properties
        lightPane.setAlignment(Pos.CENTER);
        lightPane.add(traffLight.TrafficLight(), 0, 0);
        
        buttonPane.setAlignment(Pos.CENTER_LEFT);
        buttonPane.setPadding(new Insets(5, 5, 5, 5));
        buttonPane.setHgap(5);
        buttonPane.setVgap(5);
        
        // set text fields
        
        txtgreen.setPrefWidth(60);
        txtgreen.setText("3");
        txtyellow.setPrefWidth(60);
        txtyellow.setText("3");       
        txtred.setPrefWidth(60);
        txtred.setText("3");
        
        // create and configure buttons
        Button buttonStart = new Button("Start");
        Button buttonStop = new Button("Stop");
        
        buttonPane.add(new Label("Green"), 0, 0);
        buttonPane.add(new Label("Yellow"), 0, 1);
        buttonPane.add(new Label("Red"), 0, 2);
        
        buttonPane.add(txtgreen, 1, 0);
        buttonPane.add(txtyellow, 1, 1);
        buttonPane.add(txtred, 1, 2);
        
        buttonPane.add(buttonStart, 14, 2);
        buttonPane.add(buttonStop, 15, 2);
        
        //Message field
        GridPane messagePane = new GridPane();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.setPadding(new Insets(5, 5, 5, 5));
        messagePane.setHgap(5);
        messagePane.setVgap(5);
                
        
        msgTxtField.setPrefRowCount(5);
        msgTxtField.setFont(new Font("Serif", 14));
        msgTxtField.setWrapText(false);
        msgTxtField.setEditable(false);
        
        messagePane.add(msgTxtField, 0, 1);
        
                      
        
        // text and button actions
        txtgreen.textProperty().addListener((observedVal, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                Integer.parseInt(valNew);
                if (valNew.length() > 8) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtgreen.setText(valOld);
            }
        });
        
        txtyellow.textProperty().addListener((observedVal, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                Integer.parseInt(valNew);
                if (valNew.length() > 8) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtyellow.setText(valOld);
            }
        });
        
        txtred.textProperty().addListener((observedVal, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                Integer.parseInt(valNew);
                if (valNew.length() > 8) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtred.setText(valOld);
            }
        });

        state1 = true;
        buttonStart.setOnAction((ActionEvent e ) -> {
            if (state1){
                setTimes();
                System.out.println("new values");
                System.out.println("r = " + rVal);
                System.out.println("y = " + yVal);
                System.out.println("g = " + gVal);
                //Threads
                //Create Tasks
                runnable = new RunThreads("red", rVal, yVal, gVal, traffLight, msgTxtField);
                //Create Threads
                thread = new Thread(runnable);

                thread.start();
                state1=false;
            }
        });
        
        state2 = true;
        buttonStop.setOnAction( (e) -> {
            if (state2){
                if (thread != null) {
                runnable.terminate();
                    try {
                        thread.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(NewStage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                state1=true;
            } 
        });
        
        bpane.setTop(lightPane);
        bpane.setCenter(buttonPane);
        bpane.setBottom(messagePane);
        
    return bpane;
    }
    
    public void setTimes(){
        rVal = Integer.parseInt(txtred.textProperty().getValue());
        yVal = Integer.parseInt(txtyellow.textProperty().getValue());
        gVal = Integer.parseInt(txtgreen.textProperty().getValue());
    }
    

    public void setGreen(){
        traffLight.setColor("green");
    }
    
    
    public void setYellow() {
        traffLight.setColor("yellow");
    }
    
    
    public void setRed() {
        traffLight.setColor("red");
    }
}
