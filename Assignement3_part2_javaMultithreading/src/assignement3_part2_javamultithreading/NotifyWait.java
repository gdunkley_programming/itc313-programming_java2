/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement3_part2_javamultithreading;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author graham
 */
public class NotifyWait {
    public static void main(String[] args) {
        ThreadB b= new ThreadB();
        b.start();
    
    
        synchronized(b) {
            try{
                System.out.println("waiting for b to complete...");
                b.wait();

            } catch (InterruptedException e){
                e.printStackTrace();
            }

            System.out.println("Total is: " + b.total);
        }
    
    }
}


class ThreadB extends Thread{
    int total;
    
    @Override
    public void run(){
        synchronized(this){
            for(int i=0; i<100; i++){
                total +=1;
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadB.class.getName()).log(Level.SEVERE, null, ex);
            }
            notify();
        }
    }
}