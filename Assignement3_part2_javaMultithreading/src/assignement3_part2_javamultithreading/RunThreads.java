/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement3_part2_javamultithreading;
import javafx.scene.control.TextArea;

/**
 *
 * @author graham
 */
public class RunThreads implements Runnable {
    
    int secRed;
    int secYellow;
    int secGreen;
    String currentColor;
    TrafficLight TL;
    TextArea msg;
    boolean running = true;
    
    public RunThreads(String cColor, int redtime, int yellowtime, int greentime, TrafficLight traffLight, TextArea msgTxtField) {
        
        this.secRed = redtime;
        this.secYellow = yellowtime;
        this.secGreen = greentime;
        this.currentColor = cColor;
        this.TL = traffLight;
        this.msg = msgTxtField;
    }

 
    public void terminate() {
        running = false;
    }
    
    @Override
    public void run() {
        while (running) {
            try {
                if ("red".equals(currentColor)){
                    TL.setColor(currentColor);
                    for (int i=secRed; i>=1; i--){
                        try {        
                            msg.setText("The color " + currentColor + " will change in : " + i);
                            Thread.sleep(1000);
                            
                        } catch (InterruptedException ex) {}
                    }
                    currentColor = "green";
                }
                else if ("yellow".equals(currentColor)){
                    TL.setColor(currentColor);
                    for (int i=secYellow; i>=1; i--){
                        try {        
                            msg.setText("The color " + currentColor + " will change in : " + i);
                            Thread.sleep(1000);
                            
                        } catch (InterruptedException ex) {}
                    }
                    currentColor = "red";
                }
                else if ("green".equals(currentColor)){
                    TL.setColor(currentColor);
                    for (int i=secGreen; i>=1; i--){
                        try {        
                            msg.setText("The color " + currentColor + " will change in : " + i);
                            Thread.sleep(1000);
                            
                        } catch (InterruptedException ex) {}
                    }
                    currentColor = "yellow";
                }
                
            } catch (Exception err){
            }             
        }   
    }
    
}
