/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part1;

/**
 *
 * @author graham
 */
public class TaxReportEntry {
    
    int taxID;
    double taxInc;
    double tax;
    
    public TaxReportEntry(int taxID, double taxInc, double tax){
        this.taxID = taxID;
        this.taxInc = taxInc;
        this.tax = tax;
    }
}
