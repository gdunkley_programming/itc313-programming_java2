/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;



        
/**
 *
 * @author gdunkley
 * 
 * Pseudocode:
 * steps - 
 * 1. read taxrates.txt from current directory - DONE
 * 2. store tax rate info in proper data structure (Library/MAP) - Done
 * 3. if file dosn't exist program needs to ask to provide file as an input. - DONE
 * 4. Take user inputs for Employee ID (4-digit number), - DONE ... sort of
 * 
 * 
 * annual income (floating-point number with two decimal places). - DONE
 * 5. Calculate Tax using info from both sources - Done
 * 6. output info to file with Employee ID, taxable income and tax payable into file taxreport.txt - DONE
 * 7. Above needs to be displayed in a menu format - DONE
 * 8. program to ask for another employee calculation, yes-continue, no-main menu, - DONE
 * 9. Search function on option 2 to search employee id from taxreport.txt - DONE
 * 10. if no ID ask to search again. - display message ID dosn't exist. - DONE
 * 11. if multiple, find latest/last entry -DONE
 * 12. keep searching if wanted, otherwise return to main menu. - DONE
 * 13. exit on option 3 - DONE
 * 14. Input validation also needs to be done - Scanner.hasNextXXXmethods -DONE
 * 
 * 
Taxable Income			Tax on Income
0 – $18,200				0
$18,201 – $37,000		19c for each $1 over $18,200
$37,001 – $87,000		$3,572 plus 32.5c for each $1 over $37,000
$87,001 – $180,000		$19,822 plus 37c for each $1 over $87,000
$180,001 and over		$54,232 plus 45c for each $1 over $180,000
* 
* * Employee ID   Taxable Income  Tax
1111    100000.00   20303.00
2222    90000.00    19933.00
 * 
 */

        
public class Assignement2_part1 {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        
        
        MyTaxLibrary objMyLibrary = new MyTaxLibrary();       
        Scanner taxRatesInput = objMyLibrary.OpenFile("taxrates.txt");
        Map<String, Double> taxBracket = new TreeMap<>(MyTaxLibrary.ExtractTax(taxRatesInput));
        
        Scanner taxReportInput = objMyLibrary.OpenFile("taxreport.txt");        
        
        String headderTaxReportInput = null;
        ArrayList<TaxReportEntry> taxList = new ArrayList<>();
        try{
            headderTaxReportInput = taxReportInput.nextLine(); //remove headder and store
            taxList = MyTaxLibrary.ReadToArray(taxReportInput);
            
        }catch(NoSuchElementException e){
            System.err.println("The taxreport.txt file is empty please check the file before starting again...");
            
        }

        
        Scanner usrInput = new Scanner(System.in);
        int status = 1;
        while (status == 1)
        {
            TimeUnit.MILLISECONDS.sleep(150);        
// this has been added to allow the System.out and System.err streams to be empty before displaying the main menu.
            
            System.out.print("\n\n\t               Welcome to Tax Management System of XYZ " 
                    + "\n\t ******************************************************************" 
                    + "\n\t Please select one of the following options: "
                    + "\n\t 1. Calculate tax" 
                    + "\n\t 2. Search tax"
                    + "\n\t 3. Exit & Save"
                    + "\n\t ******************************************************************\n" 
                    + "\t : " );
            System.out.flush();
            
            
            String cliInput = usrInput.next();
            switch(cliInput)
            {
                case "1":
                    String response;
                    do {
                        System.out.println("You have selected to : \nCalculate tax...\n");
                        System.out.println("Please enter the Employee ID (4-digit number)...\n");
                        System.out.print(": ");
                        int employeeID = 0;
                        try{
                            employeeID = usrInput.nextInt();

                        } catch(InputMismatchException e){

                            System.err.println("Error - Please check the Employee ID has a (4-digit number before trying again.)");
                            usrInput.next();
                            System.out.println(e);
                        }


                        if (MyTaxLibrary.InputValidation_1(usrInput, employeeID) == false){
                            break;
                        }

                        System.out.println("Please enter the annual income of the employee (no dollar sign or comma's) eg. 123400...\n");
                        System.out.print(": ");
                        if (MyTaxLibrary.InputValidation_2(usrInput) == false){
                            System.out.flush();
                            break;
                        }

                        Double employeeIncome = usrInput.nextDouble();
                        BigDecimal taxPayable = MyTaxLibrary.TaxPayable(taxBracket, employeeIncome);
                        taxPayable = taxPayable.setScale(2, RoundingMode.HALF_EVEN);                   
                        System.out.println("Tax Payable :" + taxPayable);
                        System.out.println("To ensure your data is saved to file, EXIT through option 3 to SAVE and Exit.");


                        TaxReportEntry taxRepEntryNew = new TaxReportEntry(employeeID, employeeIncome, taxPayable.doubleValue());
                        taxList.add(taxRepEntryNew);
                        
                        System.out.println("Do you want to enter another calculation? yes/no?");
                        System.out.print(": ");
                        response = usrInput.next();
                        
                    } while ("yes".equals(response) | "Yes".equals(response) | "YES".equals(response));
                    
                    break;

                case "2":
                    String response2;
                    do {
                        

                        System.out.println("You have selected to : \nSearch tax records...\n");
                        System.out.println("Please enter the Employee ID (4-digit number) to SEARCH...\n");
                        System.out.print(": ");
                        int searchEmployeeID = 0;
                        try{
                            searchEmployeeID = usrInput.nextInt();

                        } catch(InputMismatchException e){

                            System.err.println("Error - Please check the Employee ID has a (4-digit number before trying again.)");
                            usrInput.next();
                            System.out.println(e);
                        }


                        if (MyTaxLibrary.InputValidation_1(usrInput, searchEmployeeID) == false){
                            break;
                        }


                        System.out.println("The following payable tax entrys are for Employee " + searchEmployeeID);
                        System.out.println(headderTaxReportInput);
                        int count = 0;
                        String message = null;
                        for(TaxReportEntry taxVal: taxList){
                            if(taxVal.taxID == searchEmployeeID){
                                message = taxVal.taxID + "\t\t\t$" +
                                        String.format("%.2f", taxVal.taxInc) + "\t\t$" +
                                        String.format("%.2f", taxVal.tax) + "\n";


    //                            System.out.print(taxVal.taxID + "\t\t\t$");
    //                            System.out.print(String.format("%.2f", taxVal.taxInc) + "\t\t$");
    //                            System.out.println(String.format("%.2f", taxVal.tax));
                                count++;
                            }
                        }
                        

                        if(count == 0 ){
                            System.out.println("Employee ID dosn't exits... would you like to search again. yes/no?");
                        }else if (count > 0){
                            System.out.println(message);
                            System.out.println("Would you like to search again? yes/no?");
                            System.out.print(": ");
                        }
                        response2 = usrInput.next();
                        
                    } while ("yes".equals(response2) | "Yes".equals(response2) | "YES".equals(response2));
                    
                    System.out.println("\n\n");        

                    break;

                case "3":
                    System.out.println("You have selected to : \nExit...\nClosing File and Exiting...");
                    System.out.println("Writing file..");
                                       
                    try {
                        PrintWriter outputPrintWriter = new PrintWriter(new File("taxreport-new.txt"));
                        outputPrintWriter.println(headderTaxReportInput);
                        for (TaxReportEntry taxentry : taxList) {
                            
                            outputPrintWriter.print(taxentry.taxID + "\t\t");
                            outputPrintWriter.print(String.format("%.2f", taxentry.taxInc) + "\t\t");
                            outputPrintWriter.println(String.format("%.2f", taxentry.tax));
                        }
                        outputPrintWriter.close();

                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Assignement2_part1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    status = 0;
                    break;

                default:
                    
                    System.out.println("You have selected and incorrect option, Please check and run again.\n");
                    break;

            }
        }
        

    }
    
}
