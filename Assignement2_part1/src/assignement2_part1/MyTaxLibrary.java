/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement2_part1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author gdunkley
 */
public class MyTaxLibrary {
    
    public Scanner input;
        
    public Scanner OpenFile(String txtFile)
    {
        // attempt to open/read file and catch errors.
        try 
        {
            input = new Scanner(new File(txtFile));
            
            System.out.println("The file :" + txtFile + " has been read successfully.");
            System.out.println();
        } 
        catch (FileNotFoundException e) 
        {
            System.err.println("\t\tError - File: " + txtFile + " - dos'nt exist or not in directory \n" 
                    + "\t\tPlease check the filename and path before trying again.");
            System.out.println();
            System.out.println("Error is - " + e);
            System.err.flush();
                    // note not using the "e" to stop program stopping.
            System.out.println();
        }
        return input;
    }
    
//    public void WriteFile(TaxReportEntry arrayData, String txtFile){
//        
//        try {
//            PrintWriter output = new PrintWriter(new File(txtFile));
////            FileOutputStream output = new FileOutputStream(txtFile, false);
////            for (Iterator it = arrayData.iterator(); it.hasNext();) {
////                Object arrayVal = it.next();
////                output.print(arrayVal);
////            }
////            
//            for(TaxReportEntry taxentrys: arrayData){
//                output.print(taxentrys);
//            }
////                System.out.println(headderTaxReportInput);
////                for (TaxReportEntry taxentry : taxList) {
////                        System.out.println(taxentry.taxID + "\t\t\t" + taxentry.taxInc + "\t\t\t" +  taxentry.tax);
//            
//            System.out.println("The file :" + txtFile + " has been writern successfully.");
//            System.out.println();
//            
////            for (TaxReportEntry taxentry : taxList) {
////                System.out.println(taxentry.taxID + "\t\t\t" + taxentry.tax);
////
////            }
//            
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(MyTaxLibrary.class.getName()).log(Level.SEVERE, null, ex);
////            System.err.println("\t\tError - File: " + txtFile + " - dos'nt exist or not in directory \n" 
////                    + "\t\tPlease check the filename and path before trying again.");
////            System.out.println();
////            System.out.println("Error is - " + ex);
////            System.err.flush();
////                    // note not using the "e" to stop program stopping.
////            System.out.println();
//        } catch (IOException ex) {
//            Logger.getLogger(MyTaxLibrary.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
    
    
    public static ArrayList ReadToArray(Scanner scannerInput){
        
        ArrayList<TaxReportEntry> arrayReturn = new ArrayList<>();
        int empID;
        double taxInc;
        double taxPay;
        TaxReportEntry arrayEntry;
        
        
        while(scannerInput.hasNextLine()){
            empID = scannerInput.nextInt();
            taxInc = scannerInput.nextDouble();
            taxPay = scannerInput.nextDouble();
            arrayEntry = new TaxReportEntry(empID, taxInc, taxPay);
            arrayReturn.add(arrayEntry);
        }
        
        
//         TaxReportEntry taxRepEntry1 = null;
//        ArrayList<TaxReportEntry> arrayListTaxReport = new ArrayList<>();          
//        
//        int empID = 0;
//        float empTax;
//        
//        for(int i=0; i<newArray.length -1; i++){
//            for(int j=0; j<2; j++){
//                if (newArray.hasNext()){
//                    if (j==0){
//                        empID = newArray.nextInt();
//                    }
//                    else if (j==1){
//                        empTax = newArray.nextFloat();
//                        taxRepEntry1 = new TaxReportEntry(empID, empTax);
//                        arrayListTaxReport.add(taxRepEntry1);
//                    }
//                }
//            }
//        }
//        // check arraylist traverse elements of array list object
//        System.out.println(headderTaxReportInput);
//        for (TaxReportEntry taxentry : arrayListTaxReport) {
//            System.out.println(taxentry.empId + "\t\t\t" + taxentry.tax);
//        }
        return arrayReturn;
    }
    
    
    
    public static boolean InputValidation_1(Scanner usrInput, int curInput){
        boolean validation = true;
        try {
            String regex_1 = "^\\d{4}$";
            Pattern pattern_regex1 = Pattern.compile(regex_1);
//            Integer employeeID = usrInput.nextInt();
            Integer employeeID = curInput;
            Matcher match_regex1 = pattern_regex1.matcher(Integer.toString(employeeID));
            if (match_regex1.find() == false){
                System.err.println("You have entered the wrong number of digits or characters\n"
                        + "please enter the Employee ID with a (4-digit number).");
                validation = false;

            } else {
                System.out.println("correct syntax for Employee ID.");
            }         
        } catch(InputMismatchException e){
            System.err.println("Error - Please check the Employee ID has a (4-digit number before trying again.");
            validation = false;
            usrInput.next();
            System.out.println(e);
        }
        return validation;
    }
    
    public static boolean InputValidation_2(Scanner usrInput){
        boolean validation = true;
        try {
            if (usrInput.hasNextFloat() == false){
                System.err.println("You have entered the wrong number of digits or characters\n "
                        + "please enter the Employee's annual income with(no dollar sign or comma's) eg. 123400...");
                validation = false;
                usrInput.next();
            }

        } catch(InputMismatchException e){
            System.err.println("Error - \n" 
                + "Please check the Employee income is a decimal number (no dollar sign or comma's) eg. 123400.");
            validation = false;
            usrInput.next();
            System.out.println(e);
        }
        return validation;
    }
    
    public static Map<String, Double> ExtractTax(Scanner input){
        
        Map<String, Double> hashMap = new HashMap<>();        
        String regex_cents = "(\\d{2}[c])|(\\d{2}\\.\\d{1}[c])";
        String regex_dollars = "(\\d{1,3})(,\\d{1,3})";
        Pattern pattern_dollars = Pattern.compile(regex_dollars);
        Pattern pattern_cents = Pattern.compile(regex_cents);
        
        int lineNum = 1;
        input.nextLine(); //skip headding
        while(input.hasNextLine()){
            String cur_line = input.nextLine();
            Matcher matcher_dollar = pattern_dollars.matcher(cur_line);
            Matcher matcher_cents = pattern_cents.matcher(cur_line);
            int match = 1;
            
            while(matcher_dollar.find()){                
                Double dblMatch = Double.parseDouble(matcher_dollar.group().replace(",", ""));
                
                if (lineNum == 1){
                    hashMap.put("brk_start_" + lineNum, 0.00);
                    hashMap.put("brk_end_" + lineNum, dblMatch);
                    hashMap.put("brk_taxPayable_" + lineNum, 0.00);
                    hashMap.put("brk_excessThreashold_" + lineNum, 0.00);
                    hashMap.put("brk_excessThreasholdRate_" + lineNum, 0.00); 
                } else if(lineNum == 2){
                    switch (match) {
                        case 1:
                            hashMap.put("brk_start_" + lineNum, dblMatch);
                            break;
                        case 2:
                            hashMap.put("brk_end_" + lineNum, dblMatch);
                            break;
                        case 3:
                            hashMap.put("brk_excessThreashold_" + lineNum, dblMatch); 
                            break;
                        default:
                            break;
                    }
                    hashMap.put("brk_taxPayable_" + lineNum, 0.00);
                    
                } else if(lineNum == 3| lineNum == 4){
                    switch (match) {
                        case 1:
                            hashMap.put("brk_start_" + lineNum, dblMatch);
                            break;
                        case 2:
                            hashMap.put("brk_end_" + lineNum, dblMatch);
                            break;
                        case 3:
                            hashMap.put("brk_taxPayable_" + lineNum, dblMatch);
                            break;
                        case 4:
                            hashMap.put("brk_excessThreashold_" + lineNum, dblMatch); 
                            break;
                        default:
                            break;
                    }
                } else if(lineNum == 5){
                    switch (match) {
                        case 1:
                            hashMap.put("brk_start_" + lineNum, dblMatch);
                            break;
                        case 2:
                            hashMap.put("brk_taxPayable_" + lineNum, dblMatch);
                            break;
                        case 3:
                            hashMap.put("brk_excessThreashold_" + lineNum, dblMatch); 
                            break;
                        default:
                            break;
                    }
                } 
                match+=1;
            }
            while(matcher_cents.find()){
                Double dblMatchCents = Double.parseDouble(matcher_cents.group().replace("c", ""));
                dblMatchCents = dblMatchCents / 100;
                hashMap.put("brk_excessThreasholdRate_" + lineNum, dblMatchCents); 
            }
            lineNum+=1;
        }
    return hashMap;     
    }
    
    
    public static BigDecimal TaxPayable(Map<String, Double> tax_brackets, double empInc){
        BigDecimal taxpayable = new BigDecimal(0.00);
        taxpayable = taxpayable.setScale(2, RoundingMode.HALF_EVEN);        
        Map<String, Double> tbracket = tax_brackets;
       
        int brkNum;
        if(empInc < tbracket.get("brk_end_1")){
            brkNum = 1;
        }
        else if(empInc < tbracket.get("brk_end_2")){
            brkNum = 2;
        }
        else if(empInc < tbracket.get("brk_end_3")){
            brkNum = 3;
        }
        else if(empInc < tbracket.get("brk_end_4")){
            brkNum = 4;
        }
        else {
            brkNum = 5;
        }
        
        System.out.println("Applying Tax Bracket: " + brkNum);
        
        BigDecimal brk_excessThreasholdRate = new BigDecimal(tbracket.get("brk_excessThreasholdRate_" + brkNum));
        BigDecimal brk_excessThreashold = new BigDecimal(tbracket.get("brk_excessThreashold_" + brkNum));
        BigDecimal brk_taxPayable = new BigDecimal(tbracket.get("brk_taxPayable_" + brkNum));
        BigDecimal income = new BigDecimal(empInc);
        
        taxpayable = income.subtract(brk_excessThreashold);
        taxpayable = taxpayable.setScale(2, RoundingMode.HALF_EVEN);

        taxpayable = taxpayable.multiply(brk_excessThreasholdRate);
        taxpayable = taxpayable.setScale(2, RoundingMode.HALF_EVEN);

        taxpayable = taxpayable.add(brk_taxPayable);
        taxpayable = taxpayable.setScale(2, RoundingMode.HALF_EVEN);

        
        return taxpayable;
    }
    
    
}
