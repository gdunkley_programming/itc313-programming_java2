/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement3_part1_javadbinterface;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 *
 * @author graham
 */
public class Operations {
    
    
    private final Alert alertErr = new Alert(Alert.AlertType.ERROR);
    private final Alert alertWarn = new Alert(Alert.AlertType.WARNING);
    private final Alert alertInf = new Alert(Alert.AlertType.INFORMATION);
    private final JavaSqlConnector jsc = new JavaSqlConnector();
            

    public void Operation() {

        alertInf.setTitle("Informational Message");
        alertWarn.setTitle("Warning Message");
        alertErr.setTitle("Database Error");
        
    }
    
    
    public void createTable(){

        try {
            jsc.jbdcConnect();
            jsc.jdbcBuildTbl_student();

            alertInf.setContentText("Database table created");
            alertInf.showAndWait();                

        } catch (SQLException ex) {
            alertWarn.setContentText("Database table already exists");
            alertWarn.showAndWait();

        } finally {
            jsc.jdbcCloseDB();
        }
    }
    
    
    public void recordInsert(String tblname, Integer id, String name, int quiz, int a1, int a2, int a3, int exam, double result, String grade) {
        
        String columns = " (id, studentName, quiz, a1, a2, a3, exam, results, grade)";
                   
        try {
            jsc.jbdcConnect();
            String sql = "INSERT into " + tblname + columns + " values (" +
                    "'" + id + "', " +
                    "'" + name + "', " +
                    "'" + quiz + "', " +
                    "'" + a1 + "', " +
                    "'" + a2 + "', " +
                    "'" + a3 + "', " +
                    "'" + exam + "', " +
                    "'" + result + "', " +
                    "'" + grade + "')";
            
            jsc.jbdcInsert(sql);
            
        } catch (MySQLIntegrityConstraintViolationException ex){
//            System.out.println("operations catch1: " + ex);
        
        } catch (SQLException err){
//            System.out.println("operations catch2: " + err);
            alertWarn.setContentText("Database record already exists, either 'Update Record' or change details.");
            alertWarn.showAndWait();
            
        } finally {
            jsc.jdbcCloseDB();
        }
        
    }
    
    
    public void recordUpdate(String tblname, Integer id, String name, int quiz, int a1, int a2, int a3, int exam, double result, String grade) {
                           
        try {
            jsc.jbdcConnect();
            jsc.jdbcUpdatePrepared(id, name, quiz, a1, a2, a3, exam, result, grade);

            
        } catch (MySQLIntegrityConstraintViolationException ex){
//            System.out.println("operations catch1: " + ex);
        
        } catch (SQLException err){
//            System.out.println("operations catch2: " + err);
            
            alertWarn.setContentText("error: " + err.toString());
            alertWarn.showAndWait();
            
        } finally {
            jsc.jdbcCloseDB();
        }
        
    }
       
    
    public ObservableList<Student> searchStudentList(int id){
        
        ObservableList<Student> students = FXCollections.observableArrayList();
        ResultSet result;
        
        try {
            jsc.jbdcConnect();
            result = jsc.searchRecord(id);
            
            while (result.next()) {
                students.add(new Student(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4),
                result.getInt(5), result.getInt(6), result.getInt(7), result.getDouble(8), result.getString(9)));
            }
            
        } catch (SQLException e) {
         System.out.println(e);
            
        } finally {
            jsc.jdbcCloseDB();
            
        }
        return students;
    }
    
    
    public ObservableList<Student> studentList(){
        
        ObservableList<Student> students = FXCollections.observableArrayList();
        ResultSet result;
        
        try {
            jsc.jbdcConnect();
            result = jsc.studentRecords();
            
            while (result.next()) {
                students.add(new Student(result.getInt(1), result.getString(2), result.getInt(3), result.getInt(4),
                result.getInt(5), result.getInt(6), result.getInt(7), result.getDouble(8), result.getString(9)));
            }
            
        } catch (SQLException e) {
         System.out.println(e);
            
        } finally {
            jsc.jdbcCloseDB();
            
        }
        return students;
    }
    
       
    public double calculateResults(TextField q1, TextField a1, TextField a2, TextField a3, TextField e1) {
        
        int quiz = Integer.parseInt(q1.textProperty().getValue());
        int ass1 = Integer.parseInt(a1.textProperty().getValue());
        int ass2 = Integer.parseInt(a2.textProperty().getValue());
        int ass3 = Integer.parseInt(a3.textProperty().getValue());
        int exam = Integer.parseInt(e1.textProperty().getValue());
                
        double result = (quiz * 0.05) +
                (ass1 * 0.15) +
                (ass2 * 0.2) +
                (ass3 * 0.10) +
                (exam * 0.5);

        return result;
    }
    
    
    public String calculateGrade(double mark) {
        String grade;
		
        if(mark >= 85){
                grade = "HD";
        } else if (mark >= 75){
                grade = "DI";
        } else if (mark >= 65){
                grade = "CR";
        } else if (mark >= 50){
                grade = "PS";
        } else {
                grade = "FL";
        }

        return grade;
    }
    
    
    public void clearText(TextField id, TextField StuName, TextField quiz, TextField a1,
            TextField a2, TextField a3, TextField exam, TextField results, TextField grade) {
        
        id.clear();
        StuName.clear();
        quiz.clear();
        a1.clear();
        a2.clear();
        a3.clear();
        exam.clear();
        results.clear();
        grade.clear();
        
    }
    
    
    // convert TextFields from String to Integer
    public void txtConfirmInteger (TextField txtField ) {
        txtField.textProperty().addListener((valObserved, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                int mark = Integer.parseInt(valNew);
                if (mark < 0 || mark > 100) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtField.setText(valOld);
            }
        });
    }
    
}
