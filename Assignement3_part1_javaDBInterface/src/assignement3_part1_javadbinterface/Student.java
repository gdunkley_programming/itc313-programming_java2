/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignement3_part1_javadbinterface;

import javafx.beans.property.*;

/**
 *
 * @author graham
 */
public class Student {
    
    SimpleIntegerProperty id;
    SimpleStringProperty studentName;
    SimpleIntegerProperty quiz;
    SimpleIntegerProperty a1;        
    SimpleIntegerProperty a2;
    SimpleIntegerProperty a3;
    SimpleIntegerProperty exam;
    SimpleDoubleProperty results;
    SimpleStringProperty grade;
    
    
    Student(int id, String student, int quiz, int a1, 
            int a2, int a3, int exam, double results, String grade) {
        
        this.id = new SimpleIntegerProperty(id);
        this.studentName = new SimpleStringProperty(student);
        this.quiz = new SimpleIntegerProperty(quiz);
        this.a1 = new SimpleIntegerProperty(a1);        
        this.a2 = new SimpleIntegerProperty(a2);
        this.a3 = new SimpleIntegerProperty(a3);
        this.exam = new SimpleIntegerProperty(exam);
        this.results = new SimpleDoubleProperty(results);
        this.grade = new SimpleStringProperty(grade);
    }
    
    public String getName() {
            return studentName.getValue();
    }

    public int getID() {
            return id.getValue();
    }

    public int getQuiz() {
            return quiz.getValue();
    }

    public int getA1() {
            return a1.getValue();
    }

    public int getA2() {
            return a2.getValue();
    }
    
    public int getA3() {
            return a3.getValue();
    }

    public int getExam() {
            return exam.getValue();
    }

    public double getResults() {
            return results.getValue();
    }

    public void setResults(double value) {
            results.set(value);
    }

    public DoubleProperty resultsProperty() {
            return results;
    }

    public String getGrade() {
            return grade.get();
    }

    public void setGrade(String value) {
            grade.set(value);
    }

//    public StringProperty gradeProperty() {
//            return grade;
//    }   
    
    
    
}
