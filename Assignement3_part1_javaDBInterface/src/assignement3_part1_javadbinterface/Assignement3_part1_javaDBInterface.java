package assignement3_part1_javadbinterface;

import java.sql.SQLException;
import java.text.DecimalFormat;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.scene.input.MouseButton;


public class Assignement3_part1_javaDBInterface extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws SQLException {
        Scene scene = new Scene(getBorderPane(), 1000, 800);
        primaryStage.setTitle("Assignement3_part1_javaDBInterface");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    protected BorderPane getBorderPane() {
        
        // setup of button and txt fields for program within a BorderPane
        BorderPane bpane = new BorderPane();
        // set up object for operations
        Operations operation = new Operations();
        
        // setup entry pane
        GridPane entryPane = new GridPane();
        entryPane.setAlignment(Pos.CENTER_LEFT);
        entryPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5 ));
        entryPane.setHgap(5.5);
	entryPane.setVgap(5.5);
        
        // text fields
        TextField txtID = new TextField();
        TextField txtStudentName = new TextField();
        TextField txtQuiz = new TextField();
        TextField txtA1 = new TextField();
        TextField txtA2 = new TextField();
        TextField txtA3 = new TextField();
        TextField txtExam = new TextField();
        TextField txtResults = new TextField();
        TextField txtGrade = new TextField();
        TextField txtSearch = new TextField();
        txtResults.setEditable(false);
        txtGrade.setEditable(false);
        
        // set txt fields to specific data types
        operation.txtConfirmInteger(txtQuiz);
        operation.txtConfirmInteger(txtA1);
        operation.txtConfirmInteger(txtA2);
        operation.txtConfirmInteger(txtA3);
        operation.txtConfirmInteger(txtExam);
        
        txtID.textProperty().addListener((valObserved, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                Integer.parseInt(valNew);
                if (valNew.length() > 8) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtID.setText(valOld);
            }
        });
        
        
        txtSearch.textProperty().addListener((valObserved, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                Integer.parseInt(valNew);
                if (valNew.length() > 8) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                txtSearch.setText(valOld);
            }
        });
        
        
        txtResults.textProperty().addListener((valObsserved, valOld, valNew) -> {
            try {
                if (valNew.equals("")) {
                    return;
                }
                
                DecimalFormat decFormat = new DecimalFormat("#0.00");
                double doubleVal = Double.parseDouble(valNew);
                decFormat.format(doubleVal);
                                
            } catch (NumberFormatException e) {
                txtResults.setText(valOld);
            }
        });
        
        // setup buttons for marks and table creation
                
        Button buttonInsertRecord = new Button("Insert Record");
        Button buttonUpdateRecord = new Button("Update Record");
        Button buttonCalGrade = new Button("Calculate Grade");
        Button buttonCalResult = new Button("Calculate Results");
        Button buttonCreateTable = new Button("Create Java2 Table");
               
        // add txt fields & buttons into entrypane
        
        entryPane.add(new Label("Student ID (must be 8 digits)"), 0, 0);
        entryPane.add(txtID, 1, 0);
        entryPane.add(new Label("Student Name"), 0, 1);
        entryPane.add(txtStudentName, 1, 1);
        entryPane.add(new Label("Quiz Results"), 4, 0);
        entryPane.add(txtQuiz, 5, 0);
        entryPane.add(new Label("A1 Results"), 4, 1);
        entryPane.add(txtA1, 5, 1);
        entryPane.add(new Label("A2 Results"), 4, 2);
        entryPane.add(txtA2, 5, 2);
        entryPane.add(new Label("A3 Results"), 4, 3);
        entryPane.add(txtA3, 5, 3);
        entryPane.add(new Label("Exam Results"), 4, 4);
        entryPane.add(txtExam, 5, 4);
        entryPane.add(new Label("All Results"), 8, 0);
        entryPane.add(txtResults, 9, 0);
        entryPane.add(new Label("Grade"), 8, 1);
        entryPane.add(txtGrade, 9, 1);
        
        entryPane.add(buttonInsertRecord, 0, 3);
        entryPane.add(buttonUpdateRecord, 0, 4);
        entryPane.add(buttonCalResult, 1, 3);
        entryPane.add(buttonCalGrade, 1, 4);
        entryPane.add(buttonCreateTable, 9, 4);
        
        // setting up results display table
        
        TableView<Student> table = new TableView();
        table.getColumns().addAll(
                addIntCol("StudentID", "ID"),
                addStrCol("Student Name",  "name"),
                addIntCol("Quiz", "quiz"),
                addIntCol("A1", "a1"),
                addIntCol("A2", "a2"),
                addIntCol("A3", "a3"),
                addIntCol("Exam", "exam"),
                addDblCol("Results", "results"),
                addStrCol("Grade", "grade"));        
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // setting up default list
        
        ObservableList<Student> defaultdata;
        defaultdata = operation.studentList();
        table.setItems(defaultdata);

        
        // Set selection of table item to appear in fields
        
        table.setOnMouseReleased((event) -> {
            try {
                if (event.getButton().equals(MouseButton.PRIMARY)){
                    int index = table.getSelectionModel().getSelectedIndex();
                    Student studentSelction = table.getItems().get(index);

                    txtID.setText(studentSelction.id.getValue().toString());
                    txtStudentName.setText(studentSelction.studentName.getValue());
                    txtQuiz.setText(studentSelction.quiz.getValue().toString());
                    txtA1.setText(studentSelction.a1.getValue().toString());
                    txtA2.setText(studentSelction.a2.getValue().toString());
                    txtA3.setText(studentSelction.a3.getValue().toString());
                    txtExam.setText(studentSelction.exam.getValue().toString());
                    txtResults.setText(studentSelction.results.getValue().toString());
                    txtGrade.setText(studentSelction.grade.getValue());
                }   
            } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                
            }
        });

        
        // setup button pane
        
        GridPane buttonPane = new GridPane();
        buttonPane.setAlignment(Pos.CENTER_LEFT);
        buttonPane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5 ));
        buttonPane.setHgap(5.5);
	buttonPane.setVgap(5.5);
        
        Button buttonResetView = new Button("Reset View");
        Button buttonSearch = new Button("Search");

        buttonPane.add(txtSearch, 1, 0);
        buttonPane.add(buttonSearch, 2, 0);        
        buttonPane.add(buttonResetView, 4, 0);
        
        // create java connector object
        
        JavaSqlConnector jsc = new JavaSqlConnector();
        
        // create alert objects
        
        Alert alertErr = new Alert(Alert.AlertType.ERROR);
        alertErr.setTitle("Database Error");
        Alert alertWarn = new Alert(Alert.AlertType.WARNING);
        alertWarn.setTitle("Warning Message");
        Alert alertInf = new Alert(Alert.AlertType.INFORMATION);
        alertInf.setTitle("Informational Message");
        
        
        // set up events for operation buttons
        
        buttonCreateTable.setOnAction((ActionEvent e) -> {
            operation.createTable();
        });
              
        buttonInsertRecord.setOnAction((ActionEvent e) -> {
            try {

            operation.recordInsert("Java2",
                        Integer.parseInt(txtID.textProperty().getValue()),
                        txtStudentName.textProperty().getValue(),
                        Integer.parseInt(txtQuiz.textProperty().getValue()),
                        Integer.parseInt(txtA1.textProperty().getValue()),
                        Integer.parseInt(txtA2.textProperty().getValue()),
                        Integer.parseInt(txtA3.textProperty().getValue()),
                        Integer.parseInt(txtExam.textProperty().getValue()),
                        Double.parseDouble(txtResults.textProperty().getValue()),
                        txtGrade.textProperty().getValue());
            } catch (NumberFormatException err) {
//                System.out.println("number format exception hit");
                alertWarn.setContentText("All data fields require values,"
                            + " if result and grade are empty apply the 'Calculate Result' and 'Calculate Grade' buttons");
                alertWarn.showAndWait();
            } finally {
                ObservableList<Student> resetViewdata;
                resetViewdata = operation.studentList();
                table.setItems(resetViewdata);
                operation.clearText(txtID, txtStudentName, txtQuiz, txtA1, txtA2, txtA3, txtExam, txtResults, txtGrade);
            }
        });
        
        buttonUpdateRecord.setOnAction((ActionEvent e) -> {
            try {
            operation.recordUpdate("Java2",
                        Integer.parseInt(txtID.textProperty().getValue()),
                        txtStudentName.textProperty().getValue(),
                        Integer.parseInt(txtQuiz.textProperty().getValue()),
                        Integer.parseInt(txtA1.textProperty().getValue()),
                        Integer.parseInt(txtA2.textProperty().getValue()),
                        Integer.parseInt(txtA3.textProperty().getValue()),
                        Integer.parseInt(txtExam.textProperty().getValue()),
                        Double.parseDouble(txtResults.textProperty().getValue()),
                        txtGrade.textProperty().getValue());
            } catch (NumberFormatException err) {
//                System.out.println("number format exception hit");
                alertWarn.setContentText("All data fields require values,"
                            + " if result and grade are empty apply the 'Calculate Result' and 'Calculate Grade' buttons");
                alertWarn.showAndWait();
            } finally {
                ObservableList<Student> resetViewdata;
                resetViewdata = operation.studentList();
                table.setItems(resetViewdata);
                operation.clearText(txtID, txtStudentName, txtQuiz, txtA1, txtA2, txtA3, txtExam, txtResults, txtGrade);
            }
        });
        
        buttonSearch.setOnAction((ActionEvent e) -> {
            ObservableList<Student> resultStudentSearch;
            resultStudentSearch = operation.searchStudentList(Integer.parseInt(txtSearch.textProperty().getValue()));
            table.setItems(resultStudentSearch);
            operation.clearText(txtID, txtStudentName, txtQuiz, txtA1, txtA2, txtA3, txtExam, txtResults, txtGrade);
        });
        
        
        buttonResetView.setOnAction((ActionEvent e) -> {
            ObservableList<Student> resetViewdata;
            resetViewdata = operation.studentList();
            table.setItems(resetViewdata);
            operation.clearText(txtID, txtStudentName, txtQuiz, txtA1, txtA2, txtA3, txtExam, txtResults, txtGrade);
        });
        
        buttonCalResult.setOnAction((ActionEvent e) -> {
            double ans = operation.calculateResults(txtQuiz, txtA1, txtA2, txtA3, txtExam);
            txtResults.setText(Double.toString(ans));
        });
        
        
        buttonCalGrade.setOnAction((ActionEvent e) -> {
            String mark = operation.calculateGrade(Double.parseDouble(txtResults.textProperty().getValue()));
            txtGrade.setText(mark);
        });
        
         
        // add entrypane into Borderpane
        
        bpane.setTop(entryPane);
        bpane.setCenter(table);
        bpane.setBottom(buttonPane);
        	
        
        return bpane;
    }
    
// table columns with String data type
    public TableColumn<Student, String> addStrCol(String head, String field){
        TableColumn<Student, String> strCol = new TableColumn<>(head);
        strCol.setCellValueFactory(new PropertyValueFactory<>(field));
        return strCol;
    }
    
// table columns with Integer data type
    public TableColumn<Student, Integer> addIntCol(String head, String field){
        TableColumn<Student, Integer> intCol = new TableColumn<>(head);
        intCol.setCellValueFactory(new PropertyValueFactory<>(field));
        return intCol;
    }
    
// table columns with Double data type
    public TableColumn<Student, Double> addDblCol(String head, String field){
        TableColumn<Student, Double> dblCol = new TableColumn<>(head);
        dblCol.setCellValueFactory(new PropertyValueFactory<>(field));
        return dblCol;
    }
    
    
}
