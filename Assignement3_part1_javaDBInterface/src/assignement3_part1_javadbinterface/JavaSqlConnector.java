package assignement3_part1_javadbinterface;

import java.sql.*;


public class JavaSqlConnector {
    
    private Connection myConnect;
    private Statement myStatement;
    private ResultSet myResult;
    private PreparedStatement prepStatement;
    private String usr = "root";
    private String pwd = "admin";
    
    private String qryBldTbl = "CREATE TABLE `GradeProcessing`.`Java2` (\n" +
            "  `id` INT NOT NULL,\n" +
            "  `studentName` VARCHAR(45) NULL,\n" +
            "  `quiz` INT NULL,\n" +
            "  `a1` INT NULL,\n" +
            "  `a2` INT NULL,\n" +
            "  `a3` INT NULL,\n" +
            "  `exam` INT NULL,\n" +
            "  `results` DECIMAL(5,2) NULL,\n" +
            "  `grade` VARCHAR(5) NULL,\n" +
            "  PRIMARY KEY (`id`));";
    
    private String qrySelStudent = "SELECT * FROM Java2";
    
    private void JavaSqlConnector() throws ClassNotFoundException {
//        Class.forName("com.mysql.jdbc.Driver");
        
    }
    
    public void jbdcConnect() throws SQLException {
        try {
            // connection to database
//            myConnect = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/GradeProcessing", usr, pwd);
//            myConnect = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/GradeProcessing?autoReconnect=true&useSSL=false", usr, pwd);     //works with certain version
//            myConnect = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/GradeProcessing?verifyServerCertificate=false&useSSL=true", usr, pwd);     //works with certain version
            myConnect = DriverManager.getConnection("jdbc:mysql://localhost:3306/GradeProcessing?verifyServerCertificate=false&useSSL=true", usr, pwd);     //works with certain version
//            System.out.println("connected to db");
                        
            // create statement
            myStatement = myConnect.createStatement();
//            System.out.println("creating statement object");

        } catch (SQLException e) {
            System.out.println("exceptions : " + e);  
        } 
    }


    public void jdbcBuildTbl_student() throws SQLException {
        
        try {
            // create executeUpdate query
            myStatement.executeUpdate(qryBldTbl);
//            System.out.println("issued statement execute update");
            
        } catch (SQLException e) {
            System.out.println(e);
        } 
    }

    
    public void jdbcRunQuery(String qryString) {
        try {
                        
            // execute sql query
//            myResult = myStatement.executeQuery(qryString);
            myResult = myStatement.executeQuery(qrySelStudent);
//            System.out.println("issued statement query");
            
            
            // process the result set
            while (myResult.next()) {
                System.out.println(myResult.getString(1) +
                        ", " +
                        myResult.getString(2));
            }
            System.out.println("outputed query");

        } catch (SQLException e) {
            System.out.println(e);
        } 
    }
    
    public void jbdcInsert(String insertString) throws SQLException {
        
        myStatement.executeUpdate(insertString);
        
    }
    
    public void jdbcUpdatePrepared(Integer id, String stdname, int stdquiz, int stda1, int stda2, int stda3, int stdexam, double stdresult, String stdgrade) throws SQLException{
        
        String updateQry = "UPDATE Java2 SET studentName=?, quiz=?, a1=?, a2=?, a3=?,"
                + "exam=?, results=?, grade=? WHERE id=? ";
        
        prepStatement = myConnect.prepareStatement(updateQry);

        prepStatement.setString(1, stdname);
        prepStatement.setInt(2, stdquiz);
        prepStatement.setInt(3, stda1);
        prepStatement.setInt(4, stda2);
        prepStatement.setInt(5, stda3);
        prepStatement.setInt(6, stdexam);
        prepStatement.setDouble(7, stdresult);
        prepStatement.setString(8, stdgrade);
        prepStatement.setInt(9, id);

        prepStatement.executeUpdate();
    }
    
    public ResultSet searchRecord(int id) throws SQLException{
        String searchquery = "SELECT * FROM Java2 WHERE id=?";
        prepStatement = myConnect.prepareStatement(searchquery);
        
        prepStatement.setInt(1, id);
        ResultSet searchVal = prepStatement.executeQuery();
        
        return searchVal;
    }
    
    
    public ResultSet studentRecords() throws SQLException{
        
        String searchquery = "SELECT * FROM Java2";
        prepStatement = myConnect.prepareStatement(searchquery);
        ResultSet searchVal = prepStatement.executeQuery();
        
        return searchVal;
    }
    
    public void jdbcCloseDB() {
        try {
            if (myResult != null) {
                myResult.close();
//                System.err.println("myResult now closed");
            }
            if (myStatement != null) {
                myStatement.close();
//                System.err.println("myStatement now closed");
            } 
            if (myConnect != null) {
                myConnect.close();
//                System.err.println("myConnect now closed");
            }
//            System.out.println("closed everything.");
            
        } catch (SQLException e) {
            System.out.println(e);
        } 
    }

}
;